<?php 
require "animal.php";
require "ape.php";
require "frog.php";

$sheep = new Animal("shaun");

echo $sheep->getName(); // "shaun"
echo "<br>";
echo $sheep->getleg(); // 2
echo "<br>";
echo $sheep->get_cold_blooded(); // false

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

echo "<br><br>";

$sungokong = new Ape("kera sakti");
echo $sungokong->getYell(); // "Auooo"
echo "<br>";
$kodok = new Frog("buduk");
echo $kodok->getJump() ; // "hop hop"
 ?>