<?php 
/**
 * 
 */
class Frog extends Animal
{
	public $name;
	public $leg =2;
	public $jump="hop hop";
	
	function __construct($nama)
	{
		$this->name=$nama;
	}
	function getName()
	{
		return $this->name;
	}
	function getLeg()
	{
		return $this->leg;
	}
	function getJump()
	{
		return $this->jump;
	}
}
 ?>