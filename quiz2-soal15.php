CREATE TABLE customers (
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255),
    email varchar(225),
    password varchar(255),
    PRIMARY KEY (id)
); 

CREATE TABLE orders (
    id int NOT NULL AUTO_INCREMENT,
    amount varchar(255),
    customer_id int,
    PRIMARY KEY (id),
    FOREIGN KEY (customer_id) REFERENCES customers(id)
); 